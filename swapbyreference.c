
#include <stdio.h>
int input1()
{
    int a;
    printf("Enter the value of a \n");
    scanf("%d",&a);
    return a;
}
int input2()
{
    int b;
    printf("Enter the value of b \n");
    scanf("%d",&b);
    return b;
}
void swap(int *a,int *b)
{
   int temp;
   temp=*a;
   *a=*b;
   *b=temp;
   printf("In swap function a=%d and b=%d\n",*a,*b);
}
int main()
{
   int a,b;
   a=input1();
   b=input2();
   printf("In main function a=%d and b=%d\n",a,b);
   swap(&a,&b);
   printf("In main function a=%d and b=%d\n",a,b);
   return 0;
}
   