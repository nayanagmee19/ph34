 #include <stdio.h>
int get()
{
    int a;
    printf("Enter a number \n");
    scanf("%d",&a);
    return a;
}
int compute(int a,int b,int c)
{
    int minimum;
    minimum=a;
    if(b<minimum)
    minimum=b;
    if(c<minimum)
    minimum=c;
    return minimum;
}
void put(int a)
{
    printf("The minimum among these is %d",a);
    return;
}
int main()
{
    int x,y,z,smallest;
    x=get();
    y=get();
    z=get();
    smallest=compute(x,y,z);
    put(smallest);
    return 0;
}