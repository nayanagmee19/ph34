 #include <stdio.h>
void get(char str[])
{
    printf("Enter a string\n");
    gets(str);
}
void concatenate(char str1[],char str2[],char str[])
{
    int i=0,j=0;
    while(str1[i]!='\0')
    {
        str[j]=str1[i];
        i++,j++;
    }
    i=0;
    while(str2[i]!='\0')
    {
        str[j]=str2[i];
        i++;
        j++;
    }
    str[j]='\0';
}
void output(char str1[],char str2[],char str[])
{
    printf("On concatenating %s and %s we get %s\n",str1,str2,str);
}
int main()
{
    char str1[50],str2[50],str[100];
    get(str1);
    get(str2);
    concatenate(str1,str2,str);
    output(str1,str2,str);
    return 0;
}