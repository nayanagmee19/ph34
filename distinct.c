#include<stdio.h>
int getn()
{
    int n;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    return n;
}
void getarray(int n,int a[n])
{
    printf("Enter the array elements\n");
    for(int i=0;i<n;i++)
        scanf("%d",&a[i]);
}
int compute(int n,int a[n])
{
    int i,j,count=0;
    for(i=0;i<n;i++)
    {
		count++;
        for(j=i+1;j<n;j++)
        {
            if(a[i]==a[j])
			{
                count--;
                break;
            }
        }
    }
    return count;
}
void output(int res)
{
    printf("The number of distinct elements in the array is %d",res);
}
int main()
{
    int n,res;
    n=getn();
    int a[n];
    getarray(n,a);
    res=compute(n,a);
    output(res);
    return 0;
}
