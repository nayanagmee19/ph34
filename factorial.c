 #include<stdio.h>
int getn()
{
    int n;
    printf("Enter a number\n");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int i,fact=1;
    for(i=1;i<=n;i++)
    {
        fact=fact*i;
    }
    return fact;
}
void output(int n,int fact)
{
    printf("%d!=%d",n,fact);
}
int main()
{
    int n,fact;
    n=getn();
    fact=compute(n);
    output(n,fact);
    return 0;
}