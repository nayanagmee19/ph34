#include<stdio.h>
int getn()
{
    int n;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    return n;
}
void getarray(int n,int a[n])
{
    printf("Enter the array elements\n");
    for(int i=0;i<n;i++)
        scanf("%d",&a[i]);
}
void compute(int n,int a[n],int m,int b[m],int res[2])
{
	int i,j,temp=0,min;
	min=a[0];
	for(i=0;i<n;i++)
		min=(min<a[i])?min:a[i];
	while(temp<n)
	{
		for(j=0;j<m;j++)
		{
			if(b[j]!=min)
			{
				res[0]=1;
				res[1]=min;
				break;	
			}
		}
		min=sec_min(n,a,min);
		temp++;
	}
}
int sec_min(int n,int a[n],int min)
{
	int secm,i;
	secm=a[0];
	for(i=0;i<n;i++)
	{
		if(a[i]<=secm && a[i]>min)
  			secm=a[i];
	}
	return secm;
}
void output(int res[2])
{
    if(res[0]!=0)
        printf("Output:%d",res[1]);
    else
        printf("Output:NO");
}   
int main()
{
    int n,m,res[2];
    n=getn();
    int a[n];
    getarray(n,a);
    m=getn();
    int b[m];
	getarray(m,b);
    compute(n,a,m,b,res);
    output(res);
    return 0;
}