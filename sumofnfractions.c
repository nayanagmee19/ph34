#include <stdio.h>
struct fraction
{
    int nr;
    int dr;
};
int getn()
{
    int n;
    printf("How many fractions do you want to add?\n");
    scanf("%d",&n);
    return n;
}
void getarray(int n,struct fraction a[n])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("Enter the numerator and denominator fraction number %d\n",i);
        scanf("%d/%d",&a[i].nr,&a[i].dr);
    }
    return;
}
struct fraction compute(int n,struct fraction a[n])
{
    int i,max,g;
    struct fraction f;
    f.nr=0;
	for(i=0;i<n-1;i++)
	{
	 max=a[i].dr>a[i+1].dr?a[i].dr:a[i+1].dr;
	}
	for(i=0;i<n;i++)
	{
	 if(max%a[i].dr!=0)
	    max=max*a[i].dr;
	}
	f.dr=max;
    for(i=0;i<n;i++)
    {
        f.nr=f.nr+(a[i].nr*(f.dr/a[i].dr));
    }
    g=gcd(f.dr,f.nr);
    f.nr=f.nr/g;
    f.dr=f.dr/g;
    return f;
}
int gcd(int a,int b)
{
    int rem,max,min;
    max=a>b?a:b;
    min=a<b?a:b;
    while(min!=0)	
    {
     rem=max%min;
     max=min;
     min=rem;
    }
    return max;
}
void output(int n,struct fraction f,struct fraction a[n])
{
    int i;
    printf("The sum of ");
    for(i=0;i<n-1;i++)
        {
            printf("%d/%d+",a[i].nr,a[i].dr);
        }
    printf("%d/%d=%d/%d",a[n-1].nr,a[n-1].dr,f.nr,f.dr);
    return;
}
int main()
{
    struct fraction f[20];
    struct fraction fres;
    int n;
    n=getn();
    getarray(n,f);
    fres=compute(n,f);
    output(n,fres,f);
    return 0;
}
