#include <stdio.h>
int getn()
{
    int n;
    printf("How many numbers do you want to add? \n");
    scanf("%d",&n);
    return n;
}
void getarray(int n,int a[n])
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("Enter number %d \n",i);
        scanf("%d",&a[i]);
    }
}
int compute(int n,int a[n])
{
    int i,sum;
    sum=0;
    for(i=0;i<n;i++)
    {
        sum=sum+a[i];
    }
    return sum;
}
void output(int a)
{
    printf("The sum is %d",a);
    return;
}
int main()
{
    int n,arr[20],sum;
    n=getn();
    getarray(n,arr);
    sum=compute(n,arr);
    output(sum);
    return 0;
}
