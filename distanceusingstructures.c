#include <stdio.h>
#include <math.h>
struct point
 {
   float x;
   float y;
 };
struct point input()
 {
   struct point p;
   printf("Enter the co-ordinates of the point \n");
   scanf("%f %f",&p.x,&p.y);
   return p;
 }
float compute(struct point p1,struct point p2)
 {
   float a;
   a=sqrt(pow((p2.x-p1.x),2)+pow((p2.y-p1.y),2));
   return a;
 }
void output(struct point p1,struct point p2,float a)
 {
   printf("The distance between (%.2f,%.2f) and (%.2f,%.2f) is %.2f",p1.x,p1.y,p2.x,p2.y,a);
   return;
 }
int main()
 {
   struct point p1,p2;
   float res;
   p1=input();
   p2=input();
   res=compute(p1,p2);
   output(p1,p2,res);
   return 0;
 }


