#include <stdio.h>
#include <math.h>
float get1()
 {
   float x;
   printf("Enter the x co-ordinate \n");
   scanf("%f",&x);
   return x;
 }
float get2()
 {
   float y;
   printf("Enter the y co-ordinate \n");
   scanf("%f",&y);
   return y;
 }
float compute(float x1,float y1,float x2,float y2)
 {
   return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
 }
void put(float a)
 {
   printf("The distance between the two points is %f",a);
 }
int main()
 {
   float x1,x2,y1,y2,distance;
   x1=get1();
   y1=get2();
   x2=get1();
   y2=get2();
   distance=compute(x1,y1,x2,y2);
   put(distance);
   return 0;
 }