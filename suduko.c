include<stdio.h>
struct suduko
{
    int a[9][9];
};
typedef struct suduko Suduko;
int getn()
{
    int n;
    printf("How many suduko boards do you want to check?");
    scanf("%d",&n);
    return n;
}
void input(int n,Suduko su[n])
{
    int i,j,k;
    for(i=0;i<n;i++)
    {
        printf("Enter the elements of suduko board number %d\n",i+1);
        for(j=0;j<9;j++)
        {
            for(k=0;k<9;k++)
            {
                scanf("%d",&su[i].a[j][k]);
            }
        }
    }
}
void compute1(int n,Suduko su[n],int resr[n][9])
{  
    int i,j,k,temp;
	for(i=0;i<n;i++)
	for(j=0;j<9;j++)
    {
     	resr[i][j]=0;
	}
    for(i=0;i<n;i++)
    {
        for(j=0;j<9;j++)
        {
            temp=su[i].a[j][0];
            for(k=0;k<9;k++)
            {
                if(su[i].a[j][k]==temp)
                {
                    resr[i][j]=j+1;
                    break;
                }
                    
            }
        }
    }
}
void compute2(int n,Suduko su[n],int resc[n][9])
{  
    int temp,i,j,k;
	for(i=0;i<n;i++)
	for(j=0;j<9;j++)
    {
     	resc[i][j]=0;
	}
    for(i=0;i<n;i++)
    {
        for(k=0;k<9;k++)
        {
            temp=su[i].a[0][k];
            for(j=0;j<9;j++)
            {
                if(su[i].a[j][k]==temp)
                {
                    resc[i][k]=k+1;
                    break;
                }
                    
            }
        }
    }
}
void compute3(int n,Suduko su[n],int ress[n][9])
{  
    int temp1,temp2,temp3=1,i,j,k;
	for(i=0;i<n;i++)
	for(j=0;j<9;j++)
	{
    	ress[i][j]=0;
	}
    for(i=0;i<n;i++)
    {
		temp1=0;
        for(k=temp1;k<9;k++)
        {
            temp2=su[i].a[k][0];
            for(j=0;j<3;j++)
            {
                if(su[i].a[j][k]==temp2)
                {
                    ress[i][k]=temp3;
					temp3++;
                    break;
                }
                temp1=temp1+3;    
            }
        }
    }
}
void output(int n,Suduko su[n],int resr[n][9],int resc[n][9],int ress[n][9])
{
	int i,j,k,temp1=0,temp2=0;
    for(i=0;i<n;i++)
    {
		printf("\nSuduko board number:%d",i+1);
		for(j=0;j<9;j++)
		for(k=0;k<9;k++)
		{
			if(su[i].a[j][k]==0)
			{
					temp1=1;
					break;
			}
		}
		if(temp==0)
			printf("\nComplete");
		else
			printf("\nIncomplete");
        for(j=0;j<9;j++)
		{
			if(resr[i][j]!=0 || resc[i][j]!=0 || ress[i][j]!=0)
				temp2=1;
				break;			
		}
		if(temp2==0)
			printf("\nViable");
		else
			printf("\nNon Viable");
		printf("\nRows:");
		for(j=0;j<9;j++)
		{
			if(resr[i][j]!=0)
        	printf("%d ",resr[i][j]);
		}
		printf("\nColumns:");
		for(j=0;j<9;j++)
		{
			if(resc[i][j]!=0)
			printf("%d ",resc[i][j]);
		}
		printf("\nSub matrices:");
		for(j=0;j<9;j++)
		{
			if(ress[i][j]!=0)
			printf("%d ",ress[i][j]); 
		}        
    }
}
int main()
{
    int n;
    n=getn();
    Suduko su[n];
    int resc[n][9],resr[n][9],ress[n][9];
    input(n,su);    
    compute1(n,su,resr);
    compute2(n,su,resc);
    compute3(n,su,ress);
    output(n,su,resr,resc,ress);
	return 0;
}
