#include<stdio.h>
int getn()
{
    int n;
    printf("Enter a positive integer\n");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int res=0,count=0;
    while(res!=1)
    {
        if(n%2==0)
            res=n/2;
        else
            res=3*n+1;
        count++;
        n=res;
    }
    return count;
}
void output(int count)
{
    printf("The function is repeated %d times\n",count);
}
int main()
{
    int n,count;
    n=getn();
    count=compute(n);
    output(count);
    return 0;
}
    