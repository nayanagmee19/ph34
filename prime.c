#include<stdio.h>
int input()
{
    int n;
    printf("Enter a number");
    scanf("%d",&n);
    return n;
}
int check(int n)
{
    int temp=0,i;
    for(i=1;i<=n;i++)
    {
        if(n%i==0)
        {
            temp=temp+1;
        }
    }
    return temp;
}
void output(int temp,int n)
{
    if(temp==2)
    {
        printf("%d is a prime number\n",n);
    }
    else
    {
        printf("%d is a composite number\n",n);
    }
}
int main()
{
    int n,temp;
    n=input();
    temp=check(n);
    output(temp,n);
    return 0;
}