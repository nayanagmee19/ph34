#include <stdio.h>
struct fraction
{
    int nr;
    int dr;
};
struct fraction input()
{
    struct fraction f;
    printf("Enter the numerator and denominator \n");
    scanf("%d %d",&f.nr,&f.dr);
    return f;
}
struct fraction compute(struct fraction f1,struct fraction f2)
{
    struct fraction f;
    int max,i;
    f.nr=(f1.nr*f2.dr)+(f2.nr*f1.dr);
    f.dr=f1.dr*f2.dr;
    max=(f.nr>f.dr)?f.nr:f.dr;
    for(i=max;i>=1;i--)
    {
        if(f.nr%i==0 && f.dr%i==0)
        {
            f.nr=f.nr/i;
            f.dr=f.dr/i;
        }
    }
    return f;
}
void output(struct fraction f,struct fraction f1,struct fraction f2)
{
    printf("The sum of %d/%d and %d/%d is %d/%d",f1.nr,f1.dr,f2.nr,f2.dr,f.nr,f.dr);
    return;
}
int main()
{
    struct fraction f1,f2,f;
    f1=input();
    f2=input();
    f=compute(f1,f2);
    output(f,f1,f2);
    return 0;
}